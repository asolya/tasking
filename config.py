import os
basedir = os.path.abspath(os.path.dirname(__file__))

class Config:
    SECRET_KEY =  os.environ.get('SECRET_KEY')
    SQLALCHEMY_DATABASE_URI = os.environ['DATABASE_URL']
    SQLALCHEMY_COMMIT_ON_TEARDOWN = True
    SQLALCHEMY_ECHO = True
    MAIL_SERVER = 'smtp.googlemail.com'
    MAIL_USE_TLS = True
    MAIL_USERNAME = os.environ.get('DEVELOPMENT_MAIL_USERNAME')
    MAIL_PASSWORD = os.environ.get('DEVELOPMENT_MAIL_PASSWORD')
    MAIL_SUBJECT_PREFIX = "Registration"
    MAIL_SENDER = 'ADMIN'
    MAIL_PORT = 587
    BOOTSTRAP_SERVE_LOCAL = True


    @staticmethod
    def init_app(app):
        pass


class DevelopmentConfig(Config):
    DEBUG = True


class TestingConfig(Config):
    TESTING = True


config = {
    'development': DevelopmentConfig,
    'testing': TestingConfig,
    'default': DevelopmentConfig
}
