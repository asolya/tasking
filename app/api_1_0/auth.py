#!/usr/bin/python
# -*- coding: utf8 -*-

from flask import g, jsonify
from ..models import User
from .errors import forbidden, unauthorized
from . import api_v1
from flask.ext.httpauth import HTTPBasicAuth

auth = HTTPBasicAuth()


@auth.login_required
def before_request():
    if not g.current_user.confirmed:
        return forbidden('Unconfirmed account')


@auth.verify_password
def verify_password(email_or_token, password):
    if email_or_token == '':
        return False
    if password == '':
        g.current_user = User.verify_auth_token(email_or_token)
        g.token_used = True
        return g.current_user is not None
    user = User.query.filter_by(email=email_or_token).first()
    if not user:
        return False
    g.current_user = user
    g.token_used = False
    return user.verify_password(password)


@api_v1.route('/api/v1.0/token/')
@api_v1.route('/api/v1.0/token')
@auth.login_required
def get_token():
    if g.token_used:
        return unauthorized('Invalid credentials')
    return jsonify({'token': g.current_user.generate_auth_token(
                   expiration=3600), 'expiration': 3600,
                    'id': g.current_user.id,
                    'name':g.current_user.name})


@auth.error_handler
def auth_error():
    return unauthorized('Invalid credentials')
