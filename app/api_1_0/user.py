#!/usr/bin/python
# -*- coding: utf8 -*-
from flask.ext.restful import Resource, reqparse, fields, marshal
from ..models import User
from .. import db
from sqlalchemy.exc import IntegrityError

user_fields = {
    'id': fields.Integer,
    'name': fields.String,
    'email': fields.String
}


class UserAPI(Resource):

    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('email', type=str, required=True,
                                   location='json', help='Missing email')
        self.reqparse.add_argument('password',  type=str, required=True,
                                   location='json', help='Missing password')
        self.reqparse.add_argument('name', type=str,
                                   location='json', help='Missing name')
        super(UserAPI, self).__init__()

    def get(self):
        args = self.reqparse.parse_args()
        u = User.query.filter_by(email=args.email).first()
        if u is None:
            return {'error': "doesn't exist"}, 400
        if u.verify_password(args.password) is False:
            return {'error': 'invalid password'}, 400
        return marshal(u, user_fields)

    def post(self):
        args = self.reqparse.parse_args()
        if args.name is None:
            args.name = args.email
        user = User(name=args.name, password=args.password, email=args.email)
        user.confirmed = True
        db.session.add(user)
        try:
            db.session.commit()
        except IntegrityError:
            db.session.rollback()
            return {'error': "user already exists"}, 405
        return marshal(user, user_fields), 201
