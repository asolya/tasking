#!/usr/bin/python
# -*- coding: utf8 -*-

from flask import g
from ..models import Context, Point
from .errors import bad_request
from flask.ext.restful import Resource, reqparse, fields, marshal
from .auth import auth
from .. import db
from arrow import now
from sqlalchemy import and_


context_fields = {
    'id': fields.Integer,
    'title': fields.String,
    'last_change': fields.Integer,
    'points': fields.List(fields.Integer)
}


context_parser = reqparse.RequestParser()
context_parser.add_argument('last_change', type=int,
                            location='json', required=True)
context_parser.add_argument('title', type=str, location='json')


def update_context(args, context):
    if args.title:
        context.title = args.title
    if args.last_change is not None:
        context.last_change = args.last_change
    else:
        context.last_change = now().timestamp
    return context


class ContextAPI(Resource):
    decorators = [auth.login_required]

    def get(self, id):
        context = Context.query.filter_by(users=g.current_user, id=id).first()
        if context is None:
            return bad_request("Doesn't exist")
        return marshal(context.to_dict(), context_fields), 200

    def put(self, id):
        args = context_parser.parse_args()
        context = Context.query.filter_by(users=g.current_user, id=id).first()
        if context is None:
            return bad_request("Doesn't exist")
        if args.last_change < context.last_change:
            return marshal(context.to_dict(), context_fields), 400
        context = update_context(args, context)

        db.session.add(context)
        db.session.commit()
        return marshal(context.to_dict(), context_fields), 200

    def delete(self, id):
        args = context_parser.parse_args()
        context = Context.query.filter_by(users=g.current_user, id=id).first()
        if context is None:
            return bad_request("Doesn't exist")
        if args.last_change is None or args.last_change < context.last_change:
            return marshal(context.to_dict(), context_fields), 400

        context.tasks = []
        db.session.add(context)
        db.session.commit()
        db.session.delete(context)
        db.session.commit()
        return 'deleted', 204


class ContextListAPI(Resource):
    decorators = [auth.login_required]

    def post(self):
        args = context_parser.parse_args()
        context = Context(title="Context", user=g.current_user)
        context = update_context(args, context)

        db.session.add(context)
        db.session.commit()
        return marshal(context.to_dict(), context_fields), 201

    def get(self):
        contexts = Context.query.filter_by(users=g.current_user).all()
        contexts_dict = [c.to_dict() for c in contexts]
        return {'contexts': marshal(contexts_dict, context_fields)}


class ContextSyncAPI(Resource):
    decorators = [auth.login_required]

    def get(self, time):
        contexts = Context.query.filter(and_(Context.users==g.current_user,
                                             Context.last_change>=time)).all()
        contexts_dict = [c.to_dict() for c in contexts]
        return {'contexts': marshal(contexts_dict, context_fields)}
