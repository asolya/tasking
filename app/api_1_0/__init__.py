 #!/use/bin/python
 # -*- coding: utf-8 -*-
from flask.ext.restful import Api
from flask.ext.restful.representations.json import output_json

output_json.func_globals['settings'] = {'ensure_ascii': False,
                                        'encoding': 'utf8'}
from flask import Blueprint


api_v1 = Blueprint('api_v1', __name__)
api = Api(api_v1)

from .task import TaskAPI, TaskListAPI, TaskSyncAPI, AutoTaskContextAPI
from .context import ContextAPI, ContextListAPI, ContextSyncAPI
from .point import PointAPI, PointListAPI, PointSyncAPI
from .subtask import SubtaskAPI, SubtaskListAPI, SubtaskSyncAPI
from .user import UserAPI

api.add_resource(UserAPI, '/api/v1.0/user', endpoint='user')
api.add_resource(UserAPI, '/api/v1.0/user/', endpoint='user/')

api.add_resource(TaskListAPI, '/api/v1.0/tasks', endpoint='tasks_list')
api.add_resource(TaskAPI, '/api/v1.0/tasks/<int:id>', endpoint='new_task')
api.add_resource(TaskListAPI, '/api/v1.0/tasks/', endpoint='tasks_list/')
api.add_resource(TaskSyncAPI, '/api/v1.0/tasks/sync/<int:time>', endpoint='tasks_sync')
api.add_resource(TaskAPI, '/api/v1.0/tasks/<int:id>/', endpoint='new_task/')

api.add_resource(AutoTaskContextAPI, '/api/v1.0/tasks/auto', endpoint='auto')
api.add_resource(AutoTaskContextAPI, '/api/v1.0/tasks/auto/', endpoint='auto/')

api.add_resource(ContextAPI, '/api/v1.0/contexts/<int:id>', endpoint='new_context')
api.add_resource(ContextListAPI, '/api/v1.0/contexts', endpoint='contexts_list')
api.add_resource(ContextAPI, '/api/v1.0/contexts/<int:id>/', endpoint='new_context/')
api.add_resource(ContextListAPI, '/api/v1.0/contexts/', endpoint='contexts_list/')
api.add_resource(ContextSyncAPI, '/api/v1.0/contexts/sync/<int:time>', endpoint='contexts_sync')

api.add_resource(PointAPI, '/api/v1.0/points/<int:id>', endpoint='new_points')
api.add_resource(PointListAPI, '/api/v1.0/points', endpoint='points_list')
api.add_resource(PointListAPI, '/api/v1.0/points/', endpoint='points_list/')
api.add_resource(PointAPI, '/api/v1.0/points/<int:id>/', endpoint='new_points/')
api.add_resource(PointSyncAPI, '/api/v1.0/points/sync/<int:time>', endpoint='points_sync')

api.add_resource(SubtaskAPI, '/api/v1.0/subtasks/<int:id>', endpoint='new_subtasks')
api.add_resource(SubtaskListAPI, '/api/v1.0/subtasks', endpoint='subtasks_list')
api.add_resource(SubtaskListAPI, '/api/v1.0/subtasks/', endpoint='subtasks_list/')
api.add_resource(SubtaskAPI, '/api/v1.0/subtasks/<int:id>/', endpoint='new_subtasks/')
api.add_resource(SubtaskSyncAPI, '/api/v1.0/subtasks/sync/<int:time>', endpoint='subtasks_sync')
