#!/usr/bin/python
# -*- coding: utf8 -*-
from flask import g
from ..models import Point, Context
from .errors import bad_request
from flask.ext.restful import Resource, reqparse, fields, marshal
from .auth import auth
from .. import db
from arrow import now
from sqlalchemy import and_


point_fields = {
    'id': fields.Integer,
    'context': fields.Integer,
    'description': fields.String,
    'last_change': fields.Integer,
    'lat': fields.String,
    'lng': fields.String
}


point_parser = reqparse.RequestParser()
point_parser.add_argument('last_change', type=int,
                          location='json', required=True)
point_parser.add_argument('context', type=int,
                          location='json')
point_parser.add_argument('description', type=str, location='json')
point_parser.add_argument('lat', type=float, location='json', default=0)
point_parser.add_argument('lng', type=float, location='json', default=0)


def update_point(args, point):
    if args.description:
        point.description = args.description
    if args.last_change:
        point.last_change = args.last_change
    else:
        point.last_change = now().timestamp
    if args.lat:
        point.lat = args.lat
    if args.lng:
        point.lng = args.lng
    return point


class PointAPI(Resource):
    decorators = [auth.login_required]

    def get(self, id):
        point = Point.query.filter_by(users=g.current_user, id=id).first()
        if point is None:
            return bad_request("Doesn't exist")
        return marshal(point, point_fields), 200

    def put(self, id):
        args = point_parser.parse_args()
        point = Point.query.filter_by(id=id, users=g.current_user).first()
        if point is None:
            return bad_request("Point doesn't exist")
        if args.last_change < point.last_change:
            return marshal(point, point_fields), 400
        if args.context is None:
            return bad_request("Missing context")
        context = Context.query.filter_by(id=args.context,
                                          users=g.current_user).first()
        if context is None:
            return bad_request("Context doesn't exist")

        point = update_point(args, point)
        point.contexts = context

        db.session.add(point)
        db.session.commit()
        return marshal(point, point_fields), 200

    def delete(self, id):
        point = Point.query.filter_by(users=g.current_user, id=id).first()
        args = point_parser.parse_args()
        if point is None:
            return bad_request("Point doesn't exist")

        if args.last_change is None or args.last_change < point.last_change:
            return marshal(point, point_fields), 400

        db.session.delete(point)
        db.session.commit()

        return 'deleted', 204


class PointListAPI(Resource):
    decorators = [auth.login_required]

    def post(self):
        args = point_parser.parse_args()
        if args.context is None:
            return bad_request("Missing context")
        context = Context.query.filter_by(id=args.context,
                                          users=g.current_user).first()
        if context is None:
            return bad_request("Context doesn't exist")

        point = Point(users=g.current_user, contexts=context)
        point = update_point(args, point)

        db.session.add(point)
        db.session.commit()
        return marshal(point, point_fields), 201

    def get(self):
        points = Point.query.filter_by(users=g.current_user).all()
        return {"points": marshal(points, point_fields)}, 200


class PointSyncAPI(Resource):
    decorators = [auth.login_required]

    def get(self, time):
        points = Point.query.filter(and_(Point.users==g.current_user,
                                         Point.last_change>=time)).order_by(Point.last_change).all()
        return {"points": marshal(points, point_fields)}, 200
