#!/usr/bin/python
# -*- coding: utf8 -*-

from flask import g
from ..models import Subtask, Task
from .errors import bad_request
from flask.ext.restful import Resource, reqparse, fields, marshal
from .auth import auth
from .. import db
from arrow import now
from sqlalchemy import and_


subtask_fields = {
    'id': fields.Integer,
    'task': fields.Integer,
    'text': fields.String,
    'last_change': fields.Integer
}


subtask_parser = reqparse.RequestParser()
subtask_parser.add_argument('last_change', type=int,
                          location='json', required=True)
subtask_parser.add_argument('task', type=int,
                          location='json')
subtask_parser.add_argument('text', type=str, location='json')


def update_subtask(args, subtask):
    if args.text:
        subtask.text = args.text
    if args.last_change:
        subtask.last_change = args.last_change
    else:
        subtask.last_change = now().timestamp
    return subtask


class SubtaskAPI(Resource):
    decorators = [auth.login_required]

    def get(self, id):
        subtask = Subtask.query.filter_by(users=g.current_user, id=id).first()
        if subtask is None:
            return bad_request("Doesn't exist")
        return marshal(subtask, subtask_fields), 200

    def put(self, id):
        args = subtask_parser.parse_args()
        subtask = Subtask.query.filter_by(id=id, users=g.current_user).first()
        if subtask is None:
            return bad_request("Doesn't exist")
        if args.last_change < subtask.last_change:
            return marshal(subtask, subtask_fields), 400
        if args.task is not None:
            task = Task.query.filter_by(id=args.task,
                                          users=g.current_user).first()
            if task is None:
                return bad_request("Task doesn't exists")
                subtask.task = task
        subtask = update_subtask(args, subtask)

        db.session.add(subtask)
        db.session.commit()
        return marshal(subtask, subtask_fields), 200

    def delete(self, id):
        subtask = Subtask.query.filter_by(users=g.current_user, id=id).first()
        args = subtask_parser.parse_args()
        if subtask is None:
            return bad_request("Subtask doesn't exist")

        if args.last_change is None or args.last_change < subtask.last_change:
            return marshal(subtask, subtask_fields)

        db.session.delete(subtask)
        db.session.commit()

        return 'deleted', 204


class SubtaskListAPI(Resource):
    decorators = [auth.login_required]

    def post(self):
        args = subtask_parser.parse_args()
        if args.task is None:
            return bad_request("Missing task")
        task = Task.query.filter_by(id=args.task,
                                          users=g.current_user).first()
        if task is None:
            return bad_request("Task doesn't exists")

        subtask = Subtask(users=g.current_user, tasks=task)
        subtask = update_subtask(args, subtask)

        db.session.add(subtask)
        db.session.commit()
        return marshal(subtask, subtask_fields), 201

    def get(self):
        subtasks = Subtask.query.filter_by(users=g.current_user).all()
        return {"subtasks": marshal(subtasks, subtask_fields)}, 200


class SubtaskSyncAPI(Resource):
    decorators = [auth.login_required]

    def get(self, time):
        subtasks = Subtask.query.filter(and_(Subtask.users==g.current_user,
                                         Subtask.last_change>=time)).order_by(Subtask.last_change).all()
        return {"subtasks": marshal(subtasks, subtask_fields)}, 200
