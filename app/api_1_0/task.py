 #!/usr/bin/python
 # -*- coding: utf-8 -*-

from flask import current_app, g
from flask.ext.restful import Resource, reqparse, fields, marshal
from ..models import Task, Subtask, Context
from .errors import bad_request
from .. import db, cache
from arrow import now
from .auth import auth, before_request
from sqlalchemy import and_
from flask.ext.sqlalchemy import get_debug_queries
from ..classifier import predict, train, vectorizing
from time import time



id_fields = {
    'id': fields.Integer
}

task_fields = {
    'id': fields.Integer,
    'text': fields.String,
    'due_date': fields.Integer,
    'notice': fields.Integer,
    'repeat_id': fields.Integer,
    'location_notice': fields.Integer,
    'last_change': fields.Integer,
    'checked': fields.Integer,
    'subtask': fields.List(fields.Integer),
    'contexts': fields.List(fields.Integer)
}


def validate_bool(value):
    if value == 0 or value == 1:
        return value
    raise ValueError("Wrong input format")


def validate_list_int(value):
    lst = []
    if type(value) is unicode:
        value = value.strip(u" ")
        value = value.strip(u"[")
        value = value.strip(u"]")
        value = value.replace(',', ' ')
        value = value.split()
    for c in value:
        try:
            x = int(c)
            lst.append(x)
        except TypeError:
            raise ValueError("Wrong input format")
    return lst

full_task_parser = reqparse.RequestParser()
full_task_parser.add_argument('text', type=unicode, location='json')
full_task_parser.add_argument('due_date', type=int, location='json')
full_task_parser.add_argument('notice', type=validate_bool,
                              location='json')
full_task_parser.add_argument('repeat_id', type=int, location='json')
full_task_parser.add_argument('location_notice', type=validate_bool,
                              location='json')
full_task_parser.add_argument('last_change',  type=int, location='json',
                              required=True,
                              help="Need to date of last change")
full_task_parser.add_argument('checked', type=validate_bool,
                              location='json')
full_task_parser.add_argument('subtask', type=int,  location='json')
full_task_parser.add_argument('contexts', type=validate_list_int,
                              location='json')

text_parser = reqparse.RequestParser()
text_parser.add_argument('text', type=unicode, location='json')


def update_task(args, task):
    if args.text is not None:
        task.text = args.text
    if args.last_change is not None:
        task.last_change = args.last_change
    if args.due_date is not None:
        task.due_date = args.due_date
    if args.notice is not None:
        task.notice = args.notice
    if args.repeat_id is not None:
        task.repeat_id = args.repeat_id
    if args.location_notice is not None:
        task.location_notice = args.location_notice
    if args.checked is not None:
        task.checked = args.checked
    if args.last_change is not None:
        task.last_change = args.last_change
    else:
        task.last_change = now().timestamp
    return task


class TaskAPI(Resource):
    decorators = [auth.login_required]

    def __init__(self):
        self.reqparse = full_task_parser
        super(TaskAPI, self).__init__()

    def get(self, id):
        task = Task.query.filter_by(id=id, users=g.current_user).first()
        if task is None:
            return bad_request("Doesn't exist")
        return marshal(task.to_dict(), task_fields)

    def put(self, id):
        task = Task.query.filter_by(id=id, users=g.current_user).first_or_404()
        args = self.reqparse.parse_args()
        if args.last_change < task.last_change:
            return marshal(task.to_dict(), task_fields), 400

        task = update_task(args, task)

        if args.subtask is not None:
            subtask = Subtask.query.filter_by(id=args.subtask).first()
            if not subtask:
                return bad_request("Subtask doesn't exist")
            task.subtask = subtask

        if args.contexts is not None:
            contexts = []
            for context in args.contexts:
                context = Context.query.filter_by(id=context,
                                                  users=g.current_user).first()
                if context is None:
                    return bad_request("Context doesn't exists")
                contexts.append(context)
            task.contexts = []
            task.contexts = contexts

        db.session.add(task)
        db.session.commit()
        return marshal(task.to_dict(), task_fields), 200

    def delete(self, id):
        task = Task.query.filter_by(id=id, users=g.current_user).first_or_404()
        args = self.reqparse.parse_args()
        if args.last_change is not None and args.last_change < task.last_change:
            return marshal(task.to_dict(), task_fields), 400
        task.contexts = []
        # some interesting bug, sqlalchemy cann't remove rows from task_context
        # now it's working, but in future need to improve this
        db.session.add(task)
        db.session.commit()
        db.session.delete(task)
        db.session.commit()
        return 'deleted', 204



class TaskListAPI(Resource):
    decorators = [auth.login_required]

    def __init__(self):
        self.reqparse = full_task_parser
        super(TaskListAPI, self).__init__()

    def get(self):
        tasks = Task.query.filter_by(users=g.current_user).order_by(Task.last_change).all()
        tasks_dict = [t.to_dict() for t in tasks]
        return {'tasks': marshal(tasks_dict, task_fields)}

    def post(self):
        args = full_task_parser.parse_args()

        task = Task(text=args.text, user=g.current_user)

        task = update_task(args, task)

        if args.subtask is not None:
            subtask = Subtask.query.filter_by(id=args.subtask).first()
            if not subtask:
                return bad_request("Subtask doesn't exist")
            task.subtask = subtask

        if args.contexts is not None:
            for context in args.contexts:
                context = Context.query.filter_by(id=context,
                                                  users=g.current_user).first()
                if context is None:
                    return bad_request("Context doesn't exists")
                task.contexts.append(context)

        db.session.add(task)
        db.session.commit()
        id = task.user
        if args.contexts is not None:
            try:
                cache.delete_memoized(train, id)
                cache.delete_memoized(vectorizing, id)
                train(id)
            except:
                print "nope"
        return marshal(task.to_dict(), task_fields), 201




class TaskSyncAPI(Resource):
    decorators = [auth.login_required]

    def get(self, time):
        tasks = Task.query.filter(and_(Task.users==g.current_user,
                                  Task.last_change>=time)).order_by(Task.last_change).all()
        tasks_dict = [t.to_dict() for t in tasks]
        return {'tasks': marshal(tasks_dict, task_fields)}



class AutoTaskContextAPI(Resource):
    decorators = [auth.login_required]

    def __init__(self):
        self.reqparse = text_parser
        super(AutoTaskContextAPI, self).__init__()


    def post(self):
        args = self.reqparse.parse_args()
        if args.text is not None:
            tick = time()
            try:
                context = predict(g.current_user.id, args.text)
                return {"context": context[0], "time": time() - tick}
            except:
                return {"error": "can not do that"}, 400
