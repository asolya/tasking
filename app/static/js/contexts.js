 

function add_new_context()
{
    $("#new_context").show();
}

function add_new_point(id)
{
    $.ajax({
        url: "/add/point/" + id, 
        success: function(result){
            $(".add-new-point-" + id).html(result);
            $("#btn-context-"+id).hide();
            initialize();
        }});
}

function init_maps(id) {
  if (points[id][0] != undefined) {
      lat = points[id][0]["lat"];
      lng = points[id][0]["lng"];
  } else {
      lat = maps_lat;
      lng = maps_lng;
  }
  div = document.getElementById("map-canvas-" + id);
  map = new GMaps({
    div: "#map-canvas-" + id,
    lat: lat,
    lng: lng
  });
  GMaps.on('click', map.map, function(e) { 
          placeMarker(e.latLng, id);
    });
  return map;
}

function set_center_of_maps() {
  GMaps.geolocate({
    success: function(position) {
      maps_lat = position.coords.latitude;
      maps_lng = position.coords.longitude;
    },
    error: function(error) {
      alert('Geolocation failed: '+error.message);
    },
    not_supported: function() {
      alert("Your browser does not support geolocation");
    }
  }); 
}    

var maps = new Object();
var maps_lat = -12.043333;
var maps_lng = -77.028333;

$(window).load( function() {
  for(id in points) {
      maps[id] = init_maps(id);
      loadsPoints(id);
      $("#pillbox" + id).on('removed.fu.pillbox', function (e, item) {
        if (item.data != undefined)
          maps[item.data.context].markers[item.value].setMap(null);
      }); 
      $("#pillbox" + id).on('clicked.fu.pillbox', function (e, item) {
        if (item.data != undefined)
          maps[item.data.context].setCenter(item.data.lat, item.data.lng); 
      }); 
      
      // Attach a submit handler to the form
      
  }
  set_center_of_maps();
});

$( ".submit-points" ).submit(function( event ) {

    event.preventDefault();
    // Get some values from elements on the page:
    var form = $( this );
      csrf_token = form.find( "input[name='csrf_token']" ).val();
      url = form.attr( "action" );

    var points = [];
    var pillboxs = $("#pillbox" + form.attr( "id" )).pillbox("items");
    for (i in pillboxs) {
        points.push(pillboxs[i].data);
    }
    //return false;
   data = new Object();
   data["csrf_token"] = csrf_token;
     // Send the data using post


        data["points"] = points;
      $.ajax({
        beforeSend: function(xhr, settings) {
          if (!/^(GET|HEAD|OPTIONS|TRACE)$/i.test(settings.type) && !this.crossDomain) {
              xhr.setRequestHeader("X-CSRFToken", csrf_token)
          }},
          type: 'POST',
          // Provide correct Content-Type, so that Flask will know how to process it.
          contentType: 'application/json',
          // Encode your data as JSON.
          data: JSON.stringify(data),
          // This is the type of data you're expecting back from the server.
          dataType: 'json',
          url: url,
          success: function (e, j, v) {
            alert();
             
          }
  });

location.reload();
   
    // Put the results in a div
  /*  posting.done(function( data ) {
      var content = $( data ).find( "#content" );
      $( "#result" ).empty().append( content );
    });
*/
  });

function loadsPoints(id) {

  for (i = 0, len = points[id].length; i < len; i++) {
      point = points[id][i];
      title = point.title;
      lat = point.lat;
      lng = point.lng;      
      markerId = addMarker(lat, lng, id, title);
      $('#pillbox'+ id).pillbox('addItems', -1, 
      [{ text: title, value: markerId.toString(), attr: {}, 
          data: { 
                lat:lat, 
                lng:lng, 
                description:title,
                context:id
               } 
        }
      ]);
  }
}


function addMarker(lat, lng, map, title) {
  var index =  maps[map].markers.length;
  var template = $('#edit_marker_template').html();
  var content = template.replace(/{marker}/g, index).replace(/{map}/g, map).replace(/{title}/g, title);
  maps[map].addMarker({
      lat: lat,
      lng: lng,
      infoWindow: 
        { content: content },
      title_changed: function(){
          this.infoWindow.close();
      }
  });
  return index;
}

function placeMarker(position, id) {
    
    var lat = position.lat();
    var lng = position.lng();
    var index = addMarker(lat, lng, id, '');
    google.maps.event.trigger(maps[id].markers[index],'click'); 
}



$(document).on('submit', '.edit_marker', function(e) {
  e.preventDefault();

  var marker = $(this).data('marker-index');
  var map = $(this).data('map-index');

  title = $('#' + marker + '_' + map).val();
  
  if (title == "")
    title = "Точка"; 

  var template = $('#edit_marker_template').html();
  var content = template.replace(/{marker}/g, marker).replace(/{map}/g, map).replace(/{title}/g, title);
  maps[map].markers[marker].infoWindow.setContent(content);
  maps[map].markers[marker].setTitle(title);
  addPillboxItem(map, marker, title);
});

function addPillboxItem(map, marker, title) {
  $('#pillbox'+ map).pillbox('removeByValue', marker);
  $('#pillbox'+ map).pillbox('addItems', -1, 
      [{ text: title, value: marker, attr: {}, 
        data: { 
                lat:maps[map].markers[marker].position.lat(), 
                lng:maps[map].markers[marker].position.lng(), 
                description:title,
                context:map
               } 
        }
      ]);
  maps[map].markers[marker].setMap(maps[map].map);
}

function editPillboxItem(map, marker, title) {
  $('#pillbox'+ map).pillbox('removeByValue', marker);
  $('#pillbox'+ map).pillbox('addItems', -1, 
      [{ text: title, value: marker, attr: {}, 
        data: { 
                lat:maps[map].markers[marker].position.lat(), 
                lng:maps[map].markers[marker].position.lng(), 
                description:title,
                context:map
               } 
        }
      ]);
}

$("div.panel-heading").hover(
  function() {
    $( this ).addClass("active");
  }, function() {
    $( this ).removeClass("active");
  }
);


$('[data-toggle="collapse"]').click('shown.bs.collapse', function() {  
  var id = $(this).attr('id');
  setTimeout(function() { maps[id].refresh();}, 1);        
});

  $('[data-toggle="collapse"]').click('hidden.bs.collapse', function() {  
  
 //$('#gmap' + $(this).attr('id')).gmap3('destroy');
});

function editContext(context) {
  e.preventDefault();
  
}		
    
    /*
     <script type="text/javascript"
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDKtNhcfrFV_thajxvkV8b6F9wHpRdtBNs">
    <script>*/


