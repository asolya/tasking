 
$('.checkbox input.check-task').on('change', function () {
  data = new Object();
  id = $(this).data().taskId;
  data["csrf_token"] = $("#csrf_token").val();
  if ($(this).is(':checked'))
    data["checked"] = '1';
  else
    data["checked"] = '0';  
   $.ajax({
          type: 'POST',
          data: data,
          url: /checked/ + id,
          success: function (e, j, v) {             
          }
  });
});

$(".collapse").collapse({
  toggle: false
});


function addReminder() {
  $("#repeat_id").val($('#newNoticePeriod').combobox("selectedItem").value.toString());
  $("#notice").val($("#dateTimeReminder").data("DateTimePicker").date().format('X').toString());
  $("#addNotice").modal('hide');
}

$("#dateTimeDueDate").on("dp.change", function (e) {
    $('#due_date').val(e.date.format('X').toString());
});

$('button[data-task-id]').on("click", function() {
    
  id = $(this).data().taskId;
  text = $("#addSubtaskForTask" + id).val();
  if (text != "") {
    data = new Object();

    data["text"] = text;
    csrf_token = $("input[name='csrf_token']" ).val();
        data["csrf_token"] = csrf_token;

      $.ajax({
          type: 'POST',
          data: $.param(data),
          dataType: 'json',
          url: "/add/"+id+"/subtask/",
          success: function (e) {
             alert()
          },
          statusCode: {
            404: function() {
              alert( "page not found" );
            },
            201: function(data, status) {
              $("#formAddNewSubtask"+id).before(data.responseText);
            }
          }
       });
   }
});

function deleteSubtask(subtask) {  
  if ($(subtask).is(':checked') == true) {
    csrf_token = $("input[name='csrf_token']" ).val();
  
    data = new Object();
    data["csrf_token"] = csrf_token;
    id = $(subtask).data().subtaskId;
    url = "/delete/subtask/" + id;
    $.ajax({
            type: 'POST',
            data: "csrf_token=" + csrf_token,
            dataType: 'json',
            url: url,
            statusCode: {
            404: function() {
              
            },
            204: function(data, status) {
              $("div[data-subtask-id=" + id + "]").fadeOut(1000, function() { $(this).remove(); });
            }
          }
    });
  }
}

function edit_task(id)  
{
    $("#collapse"+id).collapse('hide');
    $.ajax({
        url: "/edit/" + id, 
        success: function(result){
            $("div[data-task-id="+id+"]").hide();
            $("#task_" + id).append(result);
            $("#task_" + id).show();
        }});
}

function cancelEdit(button) {
  id = $(button).data().taskId;
  $("#editForm" + id).hide();
  $("div[data-task-id="+id+"]").show();
  $("li#task_"+id).show();

  $("#task_" +id + " #editForm" ).remove();

  return false;
}

function classifing(obj) {
  text = $("#text").val();
  if (text != "") {
    data = new Object();

    data["text"] = text;
    csrf_token = $("input[name='csrf_token']" ).val();
        data["csrf_token"] = csrf_token;

      $.ajax({
          type: 'POST',
          data: $.param(data),
          dataType: 'json',
          url: "/task/auto",
          success: function (e) {
             //console.log(e);
          },
          statusCode: {
            404: function() {
              alert( "page not found" );
            },
            200: function(data, status) {
              $('.checkbox[data-context-id]').each( function() {
              $(this).checkbox("uncheck");});
              $('.checkbox[data-context-id='+data+']').checkbox("check");
            }
          }
       });
   }
}
