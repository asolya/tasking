import sys
reload(sys)
sys.setdefaultencoding('utf-8')

from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.bootstrap import Bootstrap
from flask.ext.login import LoginManager
from flask.ext.mail import Mail
from flask.ext.admin import Admin
from flask.ext.cache import Cache
from config import config
from flask.ext.admin.contrib import sqla


db = SQLAlchemy()
mail = Mail()
bootstrap = Bootstrap()
login_manager = LoginManager()
login_manager.session_protection = 'strong'
login_manager.login_view = 'auth.login'
cache = Cache(config={'CACHE_TYPE':'simple'})


def create_app(config_name):
    app = Flask(__name__)
    app.config.from_object(config[config_name])
    config[config_name].init_app(app)
    db.init_app(app)
    mail.init_app(app)
    bootstrap.init_app(app)
    login_manager.init_app(app)
    cache.init_app(app)


    from .admin import admin
    admin.init_app(app)


    from .main import main as main_blueprint
    app.register_blueprint(main_blueprint)

    from .api_1_0 import api_v1 as api_1_0_blueprint
    app.register_blueprint(api_1_0_blueprint)

    from .auth import auth as auth_blueprint
    app.register_blueprint(auth_blueprint, url_prefix='/auth')

    return app
