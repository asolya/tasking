#!/usr/bin/python
# -*- coding: utf8 -*-
from flask.ext.wtf import Form
from wtforms import StringField, PasswordField, BooleanField, SubmitField
from wtforms.validators import Required, Length, Email, Regexp, EqualTo
from wtforms import ValidationError
from ..models import User


class LoginForm(Form):
    email = StringField(u'Email', validators=[Required(), Length(1, 64),
                                             Email()])
    password = PasswordField(u'Пароль', validators=[Required()])
    remember_me = BooleanField(u'Запомнить меня')
    submit = SubmitField(u'Войти')


class RegistrationForm(Form):
    email = StringField('Email', validators=[Required(), Length(1, 64),
                                           Email()])
    username = StringField(u'Имя пользователя', validators=[
        Required(), Length(1, 64), Regexp('^[A-Za-z][A-Za-z0-9_.]*$', 0,
                                          'Usernames must have only letters, '
                                          'numbers, dots or underscores')])
    password = PasswordField(u'Пароль', validators=[
        Required(), EqualTo('password2', message=u'Пароли должны совпадать.')])
    password2 = PasswordField(u'Подтвердите пароль', validators=[Required()])
    submit = SubmitField(u'Зарегистрироваться')

    def validate_email(self, field):
        if User.query.filter_by(email=field.data).first():
            raise ValidationError(u'Email уже зарегистрирован.')

    def validate_username(self, field):
        if User.query.filter_by(name=field.data).first():
            raise ValidationError(u'Имя пользователя уже используется.')


class ChangePasswordForm(Form):
    old_password = PasswordField(u'Старый пароль', validators=[Required()])
    password = PasswordField(u'Новый пароль', validators=[
        Required(), EqualTo('password2', message=u'Пароли должны совпадать.')])
    password2 = PasswordField(u'Подтвердите новый пароль', validators=[Required()])
    submit = SubmitField(u'Обновить пароль')
