from . import db, login_manager
from arrow import now
from werkzeug.security import generate_password_hash, check_password_hash
from flask.ext.login import UserMixin
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from flask import current_app, jsonify
from sqlalchemy import event
from . import cache



task_context = db.Table('task_context',
                        db.Column('task_id',
                                  db.Integer, db.ForeignKey('tasks.id')),
                        db.Column('context_id',
                                  db.Integer, db.ForeignKey('contexts.id')))


class Task(db.Model):
    __tablename__ = 'tasks'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    text = db.Column(db.Text, nullable=False)
    due_date = db.Column(db.Integer, default=0)
    notice = db.Column(db.Integer, default=0)
    repeat_id = db.Column(db.Integer, default=0)
    location_notice = db.Column(db.Integer, default=0)
    last_change = db.Column(db.Integer)
    checked = db.Column(db.Integer, default=0)

    user = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)
    subtasks = db.relationship('Subtask', backref='tasks', cascade='all')
    context = db.relationship('Context',
                              secondary=task_context,
                              backref=db.backref('task_context'), lazy='immediate'
                              )

    def __init__(self, text, user):
        self.users = user
        self.last_change = now().timestamp
        self.text = text

    def to_dict(self):
        task = dict()
        task["id"] = self.id
        task["text"] = self.text
        task["due_date"] = self.due_date
        task["notice"] = self.notice
        task["repeat_id"] = self.repeat_id
        task["location_notice"] = self.location_notice
        task["last_change"] = self.last_change
        task["checked"] = self.checked
        if self.subtasks:
            task["subtask"] = [c.id for c in self.subtasks]
        if self.context:
            task["contexts"] = [c.id for c in self.contexts]
        return task


    def __unicode__(self):
        return self.text





class User(UserMixin, db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.Text, nullable=False)
    email = db.Column(db.Text, unique=True, nullable=False)
    password_hash = db.Column(db.String(128))
    tasks = db.relationship('Task', backref='users', cascade='all, delete-orphan')
    contexts = db.relationship('Context', backref='users', cascade='all, delete-orphan')
    points = db.relationship('Point', backref='users', cascade='all, delete-orphan')
    subtasks = db.relationship('Subtask', backref='users', cascade='all, delete-orphan')
    confirmed = db.Column(db.Boolean, default=False)

    def __init__(self, name, email, password):
        self.name = name
        self.email = email
        self.password = password

    @property
    def password(self):
        raise AttributeError('password is not a readable attribute')

    @password.setter
    def password(self, password):
        self.password_hash = generate_password_hash(password)

    def verify_password(self, password):
        return check_password_hash(self.password_hash, password)

    @login_manager.user_loader
    def load_user(user_id):
        return User.query.get(int(user_id))

    @staticmethod
    def generate_fake(count=100):
        import forgery_py
        from sqlalchemy.exc import IntegrityError

        for i in range(count):
            name = forgery_py.internet.user_name(True)
            u = User(name=name,
                     email=forgery_py.internet.email_address(user=name),
                     password='111')
            u.confirmed = True
            db.session.add(u)
            try:
                db.session.commit()
            except IntegrityError:
                db.session.rollback()

    def generate_confirmation_token(self, expiration=3600):
        s = Serializer(current_app.config['SECRET_KEY'], expiration)
        return s.dumps({'confirm': self.id})

    def generate_auth_token(self, expiration=3600):
        s = Serializer(current_app.config['SECRET_KEY'],
                       expires_in=expiration)
        return s.dumps({'id': self.id})

    @staticmethod
    def verify_auth_token(token):
        s = Serializer(current_app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except:
            return None
        return User.query.get(data['id'])

    def confirm(self, token):
        s = Serializer(current_app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except:
            return False
        if data.get('confirm') != self.id:
            return False
        self.confirmed = True
        db.session.add(self)
        return True

    def __unicode__(self):
        return self.name

    def tasks_to_clf(self):
        data = []
        for task in self.tasks:
            for context in task.contexts:
                data.append({"context":context.id, "text":task.text})
        return data



class Subtask(db.Model):
    __tablename__ = 'subtasks'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    text = db.Column(db.Text, nullable=False)
    last_change = db.Column(db.Integer)
    task = db.Column(db.Integer, db.ForeignKey('tasks.id'), nullable=False)
    user = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)

    def __init__(self, task, user, text, last_change=None):
        self.text = text
        self.tasks = task
        self.users = user
        self.last_change = now().timestamp


class Context(db.Model):
    __tablename__ = 'contexts'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    title = db.Column(db.Text, nullable=False)
    last_change = db.Column(db.Integer)
    user = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)

    points = db.relationship('Point', backref='contexts', lazy='dynamic',
                             cascade='all, delete-orphan')
    tasks = db.relationship('Task',
                            secondary=task_context,
                            backref=db.backref('contexts', lazy='dynamic'),
                            lazy='dynamic')

    def __init__(self, title, user, last_change=None):
        self.title = title
        self.users = user
        if last_change is None:
            last_change = now().timestamp
        self.last_change = last_change

    def to_dict(self):
        context = dict()
        context["title"] = self.title
        context["id"] = self.id
        context["last_change"] = self.last_change
        if self.points:
            context["points"] = [c.id for c in self.points]
        return context

    def get_json_points(self):
        points = [p.to_dict() for p in self.points]
        return points

    def __unicode__(self):
        return self.title


class Point(db.Model):
    __tablename__ = 'points'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    description = db.Column(db.Text)
    last_change = db.Column(db.Integer)
    lat = db.Column(db.String)
    lng = db.Column(db.String)
    context = db.Column(db.Integer,
                        db.ForeignKey('contexts.id'), nullable=False)
    user = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)

    def to_dict(self):
        point = dict()
        point["lat"] = self.lat
        point["lng"] = self.lng
        point["title"] = self.description
        return point

    def __unicode__(self):
        return self.description
