from flask_admin.contrib import sqla
from flask_admin.contrib.sqla import filters
from flask.ext.admin import Admin
from flask import Blueprint

from .. import db#, admin
from ..models import Task, Subtask, User, Context, Point

admin_bp = Blueprint('admin', __name__)

class UserAdmin(sqla.ModelView):
    inline_models = (User)

class TasksAdmin(sqla.ModelView):
    inline_models = (Task)

admin = Admin()
admin.add_view(sqla.ModelView(User, db.session))
admin.add_view(sqla.ModelView(Task, db.session))
admin.add_view(sqla.ModelView(Subtask, db.session))
admin.add_view(sqla.ModelView(Context, db.session))
admin.add_view(sqla.ModelView(Point, db.session))



