import pymorphy2
from pandas import DataFrame
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.naive_bayes import MultinomialNB
from models import User
from . import cache

def f_tokenizer(s):
    morph = pymorphy2.MorphAnalyzer()
    if type(s) == unicode:
        t = s.split(' ')
    else:
        t = s
    f = []
    for j in t:
        m = morph.parse(j.replace('.',''))
        if len(m) <> 0:
            wrd = m[0]
            if wrd.tag.POS not in ('NUMR','PREP','CONJ','PRCL','INTJ'):
                f.append(wrd.normal_form)
    return f


@cache.memoize()
def vectorizing(id):
    data = User.query.filter_by(id=id).first().tasks_to_clf()

    corpus = DataFrame.from_dict(data, orient="columns")
    corpus.text = corpus.text.str.lstrip()
    corpus.text = corpus.text.str.rstrip()
    corpus.text = corpus.text.str.lower()

    vectorizer = TfidfVectorizer(tokenizer=f_tokenizer)
    vectorizer.fit(corpus.text.tolist())
    return vectorizer


@cache.memoize()
def train(id):
    data = User.query.filter_by(id=id).first().tasks_to_clf()

    corpus = DataFrame.from_dict(data, orient="columns")
    corpus.text = corpus.text.str.lstrip()
    corpus.text = corpus.text.str.rstrip()
    corpus.text = corpus.text.str.lower()

    vectorizer = vectorizing(id)
    X = vectorizer.transform(corpus.text.tolist()).toarray()
    y = corpus.context.values

    clf = MultinomialNB(alpha=0.7)
    clf.fit(X, y)

    return clf


def predict(id, text):
    clf = train(id)
    vectorizer = vectorizing(id)
    X_pred = vectorizer.transform([text]).toarray()
    y_pred = clf.predict(X_pred)
    return y_pred


