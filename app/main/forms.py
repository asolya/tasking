# -*- coding: utf-8 -*-
from wtforms.validators import Required
from flask.ext.wtf import Form
from wtforms import SubmitField, FieldList, StringField, FormField, \
SelectMultipleField, BooleanField, IntegerField
from wtforms import widgets


class TaskForm(Form):
    text = StringField(u"", validators=[Required()])
    context = SelectMultipleField(u"", coerce=int,
                                  option_widget=widgets.CheckboxInput(),
                                  widget=widgets.ListWidget(prefix_label=False))
    due_date = IntegerField(default=0)
    repeat_id = IntegerField(default=0)
    location_notice = IntegerField(default=0)
    notice = IntegerField(default=0)
    submit = SubmitField(u'Сохранить')


class AddContextForm(Form):
    title = StringField(u"Название", validators=[Required()])
    submit = SubmitField(u'Сохранить')


class PointForm(Form):
    description = StringField()
    lat = StringField()
    lng = StringField()
    context = StringField()


class AddPointsForm(Form):
    points = FormField(PointForm)


class CheckTaskForm(Form):
    checked = IntegerField()


class SubtaskForm(Form):
    text = StringField()
