 #!/usr/bin/python
 # -*- coding: utf-8 -*-
from flask import render_template, redirect, url_for, abort, flash, request, jsonify
from flask.ext.login import login_required, current_user
from .forms import TaskForm, AddContextForm, AddPointsForm, PointForm,  \
    SubtaskForm, CheckTaskForm
from arrow import now
from . import main
from ..models import Task, Context, Point, Subtask
from .. import db, cache
from sqlalchemy import and_
from ..classifier import predict, train, vectorizing


@main.route('/')
@login_required
def index():
    if current_user.is_authenticated():
        tasks = Task.query.filter_by(users=current_user._get_current_object()).order_by(Task.last_change.desc()).all()
        contexts = Context.query.filter_by(users=current_user._get_current_object()).order_by(Context.title.desc()).all()
        form = TaskForm()
        form.context.choices = [(c.id, c.title) for c in contexts]
        check_form = CheckTaskForm()
        return render_template('index.html', tasks=tasks, add_new=form, \
                               contexts=contexts, edit=form, check=check_form)
    return render_template('index.html')


@main.route('/today')
@login_required
def today():
    if current_user.is_authenticated():
        today = now().floor("day")
        tasks = Task.query.filter(and_(Task.users==current_user._get_current_object(),
                                       Task.due_date>=today.timestamp,
                                       Task.due_date < today.replace(days=+1).timestamp)).order_by(Task.last_change.desc()).all()
        contexts = Context.query.filter_by(users=current_user._get_current_object()).order_by(Context.title.desc()).all()
        check_form = CheckTaskForm()
        return render_template('today.html', tasks=tasks, today=today, \
                               contexts=contexts, check=check_form)


@main.route('/week')
@login_required
def week():
    if current_user.is_authenticated():
        today = now().floor("day")
        tasks = Task.query.filter(and_(Task.users==current_user._get_current_object(),
                                       Task.due_date>=today.timestamp,
                                       Task.due_date < today.replace(days=+7).timestamp)).order_by(Task.last_change.desc()).all()
        contexts = Context.query.filter_by(users=current_user._get_current_object()).order_by(Context.title.desc()).all()
        check_form = CheckTaskForm()
        return render_template('week.html', tasks=tasks, today=today, \
                               contexts=contexts, check=check_form)


@main.route('/context/<int:id>')
@login_required
def context(id):
    if current_user.is_authenticated():
        context = Context.query.filter_by(users=current_user._get_current_object(),
                                          id=id).first_or_404()
        tasks = Task.query.filter(and_(Task.users==current_user._get_current_object(),
                                       Task.contexts.contains(context))).order_by(Task.last_change.desc()).all()
        contexts = Context.query.filter_by(users=current_user._get_current_object()).order_by(Context.title.desc()).all()
        check_form = CheckTaskForm()
        return render_template('context.html', tasks=tasks, context=context, \
                               contexts=contexts, check=check_form)


@main.route('/add', methods=['GET', 'POST'])
@login_required
def add():
    contexts = Context.query.filter_by(users=current_user._get_current_object()).order_by(Context.title.desc()).all()
    form = TaskForm()
    form.context.choices = [(c.id, c.title) for c in contexts]
    if form.validate_on_submit():
        task = Task(text=form.text.data,
            user=current_user._get_current_object())
        task.due_date = form.due_date.data
        task.location_notice = form.location_notice.data
        task.repeat_id = form.repeat_id.data
        task.notice = form.notice.data

        for id in form.context.data:
            context = Context.query.filter_by(id=id).first()
            task.context.append(context)
        db.session.add(task)
        db.session.commit()
        id = task.user
        print form.context.data
        if form.context.data:
            try:
                cache.delete_memoized(train, id)
                cache.delete_memoized(vectorizing, id)
            except:
                print "nope"
        return redirect(url_for('.index'))
    return redirect(url_for('.index'))


@main.route('/delete/<int:id>', methods=['GET', 'POST'])
@login_required
def delete(id):
    task = Task.query.filter_by(id=id,
                                users=current_user._get_current_object()).first()
    task.contexts = []
    db.session.add(task)
    db.session.commit()
    db.session.delete(task)
    db.session.commit()
    return redirect(url_for('.index'))


@main.route('/checked/<int:id>', methods=['POST'])
@login_required
def check_task(id):
    form = CheckTaskForm()

    if form.validate_on_submit():
        task = Task.query.filter_by(id=id,
                                    users=current_user._get_current_object()).first()
        print form.checked.data
        if form.checked.data == 1:
            task.checked = 1
        else:
            task.checked = 0

        db.session.add(task)
        db.session.commit()
        return str(task.checked) , 200
    return 'error', 404


@main.route('/edit/<int:id>', methods=['GET', 'POST'])
@login_required
def edit(id):
    contexts = Context.query.filter_by(users=current_user._get_current_object()).order_by(Context.title.desc()).all()
    form = TaskForm()
    form.context.choices = [(c.id, c.title) for c in contexts]
    if form.validate_on_submit():
        task = Task.query.filter_by(id=id,
                        users=current_user._get_current_object()).first_or_404()
        task.text = form.text.data
#     2015-02-25 16:29:48.928525
        task.last_change = now().timestamp
        task.repeat_id = form.repeat_id.data
        task.due_date = form.due_date.data
        task.location_notice = form.location_notice.data
        task.notice = form.notice.data
        for id in form.context.data:
            context = Context.query.filter_by(id=id,
                                              users=current_user._get_current_object()).first()
            #if not task.context.contains(context):
            task.context.append(context)



        db.session.add(task)
        db.session.commit()

        return redirect(url_for('.index'))
    task = Task.query.filter_by(id=id,
                    users=current_user._get_current_object()).first_or_404()
    form = TaskForm(obj=task)
    form.context.choices = [(c.id, c.title) for c in contexts]
    form.context.data = [c.id for c in task.context]
    return render_template('_edit.html', edit=form,
                           url=url_for('main.edit', id=id), contexts=contexts, task=task)


@main.route('/add/<int:id>/subtask/', methods=['POST'])
@login_required
def add_subtask(id):
    form = SubtaskForm()
    print form
    if form.validate_on_submit():
        task = Task.query.filter_by(id=id,
                    users=current_user._get_current_object()).first_or_404()

        subtask = Subtask(text=form.text.data,
                          user=current_user._get_current_object(),
                          task=task)
        db.session.add(subtask)
        db.session.commit()
        return render_template("_subtask.html", subtask=subtask), 201

@main.route('/delete/subtask/<int:id>', methods=['POST'])
@login_required
def delete_subtask(id):
    form = SubtaskForm()
    if form.validate_on_submit():
        subtask = Subtask.query.filter_by(id=id,
                    users=current_user._get_current_object()).first_or_404()
        db.session.delete(subtask)
        db.session.commit()
        return "", 204
    return "not found", 404


@main.route('/contexts/', methods=['GET', 'POST'])
@login_required
def contexts():
    contexts = Context.query.filter_by(
                           users=current_user._get_current_object()).all()
    form = AddContextForm()
    points = AddPointsForm()
    return render_template('contexts.html', contexts=contexts,
                           add_context=form, add_points=points)


@main.route('/add/context/', methods=['GET', 'POST'])
@login_required
def add_context():
    form = AddContextForm()
    if form.validate_on_submit():
        context = Context(title=form.title.data,
                          user=current_user._get_current_object())
        db.session.add(context)
        db.session.commit()
        return redirect(url_for('.contexts'))


@main.route('/edit/context/<int:id>', methods=['POST'])
@login_required
def edit_context(id):
    form = AddContextForm()
    if form.validate_on_submit():
        context = Context.query.filter_by(id=id,
                          users=current_user._get_current_object()).first_or_404()
        context.title = form.title.data
        db.session.add(context)
        db.session.commit()
        return redirect(url_for('.contexts'))


@main.route('/delete/context/<int:id>', methods=['GET', 'POST'])
@login_required
def delete_context(id):
    context = Context.query.filter_by(id=id,
                           users=current_user._get_current_object()).first()
    context.tasks = []
    db.session.add(context)
    db.session.commit()
    db.session.delete(context)
    db.session.commit()
    return redirect(url_for('.contexts'))


@main.route('/context/<int:context_id>/points/', methods=['POST'])
@login_required
def add_points(context_id):
    form = PointForm() #.from_json(request.json)
    if form.validate_on_submit():
        points = request.get_json()["points"]
        context = Context.query.filter_by(id=context_id,
                        users=current_user._get_current_object()).first_or_404()

        print "contextnot  nulls"
        context.points = []
        print "context nulls"
        for point in points:
            point = Point(description=point["description"],
                        lat=point["lat"],
                        lng=point["lng"],
                        last_change=now().timestamp,
                        contexts=context,
                        users=current_user._get_current_object())
            db.session.add(point)
        db.session.commit()
        return "ok" # redirect(url_for('.contexts'))
    return "fails"  # redirect(url_for('.index'))


@main.route('/context/points/<int:id>', methods=['GET', 'POST'])
@login_required
def show_points(id):
    context = Context.query.filter_by(id=id,
                                      users=current_user._get_current_object()).first_or_404()

    points = Point.query.filter_by(context=id,
                                  users=current_user._get_current_object()).order_by(Point.description).all()

    return render_template('_show_points.html', points=points)


@main.route('/task/auto', methods=['POST'])
@login_required
def classify():
    form = TaskForm()
    if form.validate_on_submit():
        context = predict(current_user._get_current_object().id, form.text.data)
        return str(context[0])
