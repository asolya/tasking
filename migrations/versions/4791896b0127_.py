"""empty message

Revision ID: 4791896b0127
Revises: 3278feb65a1e
Create Date: 2015-05-11 20:34:19.064664

"""

# revision identifiers, used by Alembic.
revision = '4791896b0127'
down_revision = '3278feb65a1e'

from alembic import op
import sqlalchemy as sa


def upgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.add_column('points', sa.Column('user', sa.Integer(), nullable=False))
    op.alter_column('points', 'context',
               existing_type=sa.INTEGER(),
               nullable=True)
    op.create_foreign_key(None, 'points', 'users', ['user'], ['id'])
    op.add_column('subtasks', sa.Column('user', sa.Integer(), nullable=False))
    op.alter_column('subtasks', 'task',
               existing_type=sa.INTEGER(),
               nullable=True)
    op.create_foreign_key(None, 'subtasks', 'users', ['user'], ['id'])
    ### end Alembic commands ###


def downgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint(None, 'subtasks', type_='foreignkey')
    op.alter_column('subtasks', 'task',
               existing_type=sa.INTEGER(),
               nullable=False)
    op.drop_column('subtasks', 'user')
    op.drop_constraint(None, 'points', type_='foreignkey')
    op.alter_column('points', 'context',
               existing_type=sa.INTEGER(),
               nullable=False)
    op.drop_column('points', 'user')
    ### end Alembic commands ###
