import unittest
from app.models import User

class UserModelTestCase(unittest.TestCase):
    def test_password_setter(self):
        u = User(name='test', email='test@test', password = 'dogs')
        self.assertTrue(u.password_hash is not None)

    def test_password_getter(self):
        u = User(name='test', email='test@test', password = 'dogs')
        with self.assertRaises(AttributeError):
            u.password

    def test_password_verification(self):
        u = User(name='test', email='test@test', password = 'dogs')
        self.assertTrue(u.verify_password('dogs'))
        self.assertFalse(u.verify_password('cats'))

    def test_password_salts_are_random(self):
        u = User(name='test', email='test@test', password = 'dogs')
        u1 = User(name='test', email='test@test', password = 'dogs')
        self.assertTrue(u.password_hash != u1.password_hash)
